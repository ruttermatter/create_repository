#!/bin/bash

#: Title       : create_repository
#: Date        : 2018-01-02
#: Author      : Matthew Rutter <shellscripts@matt-rutter.com>
#: Version     : 1.0
#: Description : Script called to gather persons version control information for the creation of project directories.
#: Options     : None


#TODO
#1:Implement with functions


#Get Version Control Accounts Used
accounts=$(Multi_Select_List.sh "Which git accounts do you want to associate with this project?" bitbucket github)

project_dir="$1"

for account in $accounts
do 

    case $account in                          # If the return status is
        1) account='bitbucket';;             # 0 then set the string that says its private to true
        2) account='github';;            # 1 then set the string that says its private to false
    esac
    
    #Get Username
    username=$(Get_User_Input.sh "Please enter your $account username")

    #Get Repository Name
    reponame=$(Get_User_Input.sh "Please enter the new $account repository name")

    case $account in 

        bitbucket)

            #Get Bitbucket repsitory privacy status from user
            privacy=$(Yes_or_No.sh 'Is the repository private?')  

            case $privacy in                          # If the return status is
                0) private='true';;             # 0 then set the string that says its private to true
                1) private='false';;            # 1 then set the string that says its private to false
            esac

            git_site="https://bitbucket.org/$username/$reponame"

            clear

            #Use Bitbucket API to create the remote repository
            curl --user $username https://api.bitbucket.org/1.0/repositories/ --data name="$reponame" --data is_private=$private

            clear
        ;;

        github)

            git_site="https://github.com/$username/$reponame"

            clear

            #Use Github API to create the remote repository
            curl -u $username https://api.github.com/user/repos -d "{\"name\":\"$reponame\"}"

            clear
        ;;

    esac

    cd $project_dir

    if [ ! -d .git ]
    then 
        git init
        git add .
    fi

    git remote add "$account" "$git_site"
    git commit -m "initial commit"
    git push -u "$account" master
    
done